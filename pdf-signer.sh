#!/usr/bin/env bash
#
# Stamp PDF documents and remove their metadata for online sharing.
#
# Author  : Victor Lasa (vowkin)
# Updated : 22/07/2019
# Version : 1.0

################################################################################
# Preparation:
# Convert the scanned signature into a PDF file):
# convert signature-waltdisney-scan.png signature-waltdisney-scan.pdf
#
# Place the signature PDF file into an A4 blank document. Please note you will
# have to change the scale and offset values according to the file you want to
# stamp:
# pdfjam --paper 'a4paper' --scale 0.25 --offset '3cm -0.5cm' \
#        --outfile signature-waltdisney.pdf signature-waltdisney-scan.pdf
#
# Export the `$SIGNATURE` variable to match the path of your PDF signature:
# export SIGNATURE=~/signature-waltdisney.pdf
################################################################################

FILE=( "$@" )
count=1
total=$#
declare -a EXPORT=( )

clear=$(tput sgr0)
red=$(tput setaf 1)
green=$(tput setaf 2)

function usage() {
echo "\
Usage: $0 [FILES]...
Stamp PDF documents and remove their metadata for online sharing.

Please note that this tool can be used with as many files as needed at once.
Therefore, the use of the wildcard character (*) is encouraged.

Also note that the metadata removing performed by this tool is not as exhaustive
as it could be, but it should be enough for most cases. Should you need a more
thorough approach, using mat2 (https://0xacab.org/jvoisin/mat2) is recommended.

Examples:
  $0 document.pdf
  $0 ~/Documents/project/*.pdf"
}

# Print usage and exit if no arguments are provided
[ $# = 0 ] && usage && exit 1

tput civis

for file in "${FILE[@]}"; do
  if [ "$(file -b --mime-type "$file" 2>/dev/null)" == "application/pdf" ]; then
    if [[ "$file" != *"_signed.pdf" ]]; then
      DIRNAME=$(dirname "$file")
      BASENAME=$(basename "$file" .pdf)_signed

      printf '[%02d/%02d]' "$count" "$total"

      # Join the original ($@) and signature ($SIGNATURE) PDF files:
      printf '\033[9`%s' "Exporting signed document..."
      pdftk "$file" stamp "$SIGNATURE" output "$DIRNAME/$BASENAME".pdf 2>/dev/null
      printf '\033[38`%s\n' "${green}[✓]${clear} ('$(basename "$file")')"

      # Remove metadata
      printf '\033[9`%s' "Removing metadata..."
      exiftool -quiet -all:all= "$DIRNAME/$BASENAME.pdf" -out "$DIRNAME/$BASENAME"_clean.pdf 2>/dev/null
      qpdf --linearize "$DIRNAME/$BASENAME"_clean.pdf "$DIRNAME/$BASENAME"_linear.pdf
      mv "$DIRNAME/$BASENAME"_linear.pdf "$DIRNAME/$BASENAME".pdf
      rm "$DIRNAME/$BASENAME"_clean.pdf
      printf '\033[38`%s\n' "${green}[✓]${clear}"

      EXPORT+=( "$BASENAME".pdf )
      (( count++ ))
    else
      printf '[%02d/%02d]' "$count" "$total"
      printf '\033[9`%s' "File already signed" # Signed file already exists
      printf '\033[38`%s\n' "${red}[✗]${clear} ('$(basename "$file")')"
      (( count++ ))
    fi
  else
    printf '[%02d/%02d]' "$count" "$total"
    printf '\033[9`%s' "Not a PDF file"
    printf '\033[38`%s\n' "${red}[✗]${clear} ('$file')"
    (( count++ ))
  fi
done

if [ "${#EXPORT[@]}" -ne 0 ]; then
  printf '\n[+] Exported files:\n'
  for exported in "${EXPORT[@]}"; do
    printf "[+] '%s'\\n" "$exported"
  done
fi

tput cnorm
