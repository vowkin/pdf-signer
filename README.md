# PDF Signer

Stamp PDF documents and remove their metadata for online sharing.

## Description

This tool can be useful for automating the stamping of files in which some kind of signature is required (e.g., bills, receipts, etc.).
Keep in mind that the tool is designed to always place the signature on the same location inside the document as that was my only need at the time.

Please note that this tool can be used with as many files as needed at once.
Therefore, the use of the wildcard character (*) is encouraged.

Also note that the metadata removing performed by this tool is not as exhaustive as it could be, but it should be enough for most cases.
Should you need a more thorough approach, using mat2 (<https://0xacab.org/jvoisin/mat2>) is recommended.

## Required packages

- imagemagick
- pdfjam (texlive)
- pdftk
- exiftool
- qpdf

## Preparation

1. Convert the scanned signature into a PDF file):

   `convert signature-waltdisney-scan.png signature-waltdisney-scan.pdf`

2. Place the signature PDF file into an A4 blank document (*):

   `pdfjam --paper 'a4paper' --scale 0.25 --offset '3cm -0.5cm' --outfile signature-waltdisney.pdf signature-waltdisney-scan.pdf`

3. Export the `$SIGNATURE` variable to match the path of your PDF signature:

   `export SIGNATURE=~/signature-waltdisney.pdf`

(*) Please note that you will have to change the scale and offset values according to the file you want to stamp.

## Usage

`./pdf-signer.sh [FILES]...`

Examples:

- `./pdf-signer.sh document.pdf`
- `./pdf-signer.sh ~/Documents/receipts/*.pdf`

## Example

![Example](/example/example.png)
